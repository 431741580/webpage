const global_defaultstring = '<p style="text-align:center;font-size:30px">Woot! Saving Works!</p><p style="text-align:left;font-size:20px">Text here!</p>';

function init()
{
	var linkHolder = left_Section.children.Link_Container.children;
	window.global_textTimer = false;
	window.global_saved = true; // main.innerText is always auto loaded
	window.global_file_loaded = false;
	window.global_current_tile = 0;
	window.global_defaultstring = global_defaultstring;
	var x = localStorage.getItem("newtab_tabPage");
	var n;
	var para = Footer.children[0];
	function tabs () {
		var l = linkHolder.length;
		for (i = 0; i < l; i++)
		{
			linkHolder[i].hidden = true;
		}
		linkHolder[n].hidden = false;
	}
	if (x !== null)
	{
		n = parseInt(x, 10);
	}
	else
	{
		n = 0;
	}
	window.requestAnimationFrame(tabs);
	x = localStorage.getItem("newtab_text");
	const errorString = '<p style="text-align:center;font-size:30px">No program trace detected!</p><p style="text-align:left;font-size:20px">You may have gone Incognito</p><p style="text-align:left;font-size:20px">Note: Saving may not work correctly. Ignore this message if this is your first time visiting this page</p>';
	if (x !== null)
	{
		window.requestAnimationFrame(function () {window.Main.innerHTML = x;});
		if (x != global_defaultstring)
		{
			global_saved = false;
		}
	}
	else
	{
		if (localStorage.getItem('newtab') !== null)
		{
			window.requestAnimationFrame(function () {window.Main.innerHTML = global_defaultstring;});
		}
		else
		{
			localStorage.setItem('newtab', 1); //incognito mode detection
			if (localStorage.getItem('newtab_save-name') === null) {
				window.requestAnimationFrame(function () {window.Main.innerHTML = errorString;});
			}
			else {
				window.requestAnimationFrame(function () {window.Main.innerHTML = global_defaultstring;});
			}
		}
	}
	window.requestAnimationFrame(function () {
		para.style.webkitMarginAfter = "0px";
		para.style.webkitMarginBefore = "0px";
	});
	var phrases =
	[
		"Code blatantly ripped off W3Schools",
		"Now avaliable in English!",
		"Cause adding that sidebar was totally neccessary",
		"JavaScript has element.animate. VB sucks",
		"Now stealing all your metadata",
		"Google, fix your bloody garbage collector",
		"Episode 2: The zombie awakens"
	];
	var c = 3; //Math.floor(Math.random() * phrases.length);
	window.requestAnimationFrame(function () {Footer.innerHTML = "<p>" + phrases[c] + "</p>";window.Explorer.hidden = 1;});
	console.log("Screen elements initialised");

}
	// Constants
	//main_padding = 22
	//options_padding = 20
	//left_width = Math.floor(innerWidth * .25) + 30
	//main_partial = innerWidth - left_width - main_padding + "px" 10 = total padding, 2 = border
	//options_partial = innerWidth - left_width  - options_padding + "px" // 20 = total padding
	//main_full = innerWidth - main_padding + "px"
	//options_full = innerWidth - options_padding + "px"


		//window.Main.style.height = innerHeight - footer_offsetHeight - options_offsetHeight - 2+ "px"
		//left.style.height = innerHeight - footer_offsetHeight + "px"
		//explorer.style.height = innerHeight - footer_offsetHeight - options_offsetHeight - 2+ "px"


function deferredInit () {
	// Working around ownership and dynamic lookups
	var links = document.getElementsByTagName('a');

	var button_tabs = window.button_holder.children;
	var length = button_tabs.length;
	function parse () {tabPage(parseInt(this.value, 10));}
	for (i = 0; i < length; i++) {
		button_tabs[i].onclick = parse;
	}
	var l = window.options_bar.children;
	l[0].onclick = sideHide;
	l[1].onclick = fixedFont;
	l[2].onclick = resetMainInit;
	l[3].onclick = textSaveInit;
	l[4].onclick = textLoadInit;
	l[5].onclick = textRemoveInit;
	window.Main.oninput = textWait;
	document.body.onunload = function () {localStorage.setItem('newtab_text', window.Main.innerHTML);};
	length = links.length;
	for (i=0; i < length; i++) {
		links[i].rel = "noreferrer";
	}
	tileNavigation.addEventListener('change', navigate);
	searchButton.onclick = search;
	console.log('Script elements initialised');
}
window.setTimeout( function () {init();}, 20);
window.setTimeout( function () {deferredInit();}, 200);
