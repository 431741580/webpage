function init()
{
	"use strict"
	let linkHolder = window.Link_Container.children;
	let x = localStorage.getItem("newtab_tabPage");
	let n = parseInt(x) || 0;
	
	const tabs = function () {
		let l = linkHolder.length;
		for (i = 0; i < l; i++)
		{
			linkHolder[i].hidden = true;
		}
		linkHolder[n].hidden = false;
	};
	window.requestAnimationFrame(tabs);
	console.log("Screen elements initialised");
	
}
function deferredInit () {
	// Working around ownership and dynamic lookups
	var links = document.getElementsByTagName('a');
	
	var button_tabs = window.button_holder.children[0].children[0].children;
	var length = button_tabs.length;
	const parse = function () {console.log(this);tabPage(parseInt(this.value, 10));}
	for (i = 0; i < length; i++) {
		button_tabs[i].children[0].onclick = parse;
	}
	length = links.length;
	for (i=0; i < length; i++) {
		links[i].rel = "noreferrer";
	}
	console.log('Script elements initialised');
}
window.setTimeout( function () {init();deferredInit();}, 200);

function tabPage(n) 
{
	"use strict"
	console.log(n)
	let items = window.Link_Container.children;
	let l = items.length;
	let active;
	let active_animation;
	let focus = items[n];
	let focus_animation;
	let duration = 250;
	let rect;
	let x;
	for (x = 0; x < l; x++)
	{
		if (items[x].hidden === false) {
			active = items[x];
		}
	}
	if (active != focus) {
		rect = active.getBoundingClientRect();
		focus.style.position = "absolute";
		active.style.position = "absolute";
		focus.style.left = rect.left + "px";
		focus.style.top = rect.top + "px";
		active.style.left = rect.left + "px";
		active.style.top = rect.top + "px";
		focus.style.zIndex = 1;
		active.style.zIndex = 0;
		active_animation = opacityAnimate(focus, active, duration);
		active_animation.onfinish = function () {
			active.style.position = "static";
			active.style.top = "0px";
			active.style.left = "0px";
			focus.style.position = "static";
			focus.style.top = "0px";
			focus.style.left = "0px";
		};
		localStorage.setItem("newtab_tabPage", n);
	}
}
