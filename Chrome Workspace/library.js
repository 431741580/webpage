function timeDif(Date1, Date2)
{
	// Date1 has to be larger than Date2
	var dict = {0: 31, 1: 28, 2: 31, 3: 30, 4: 31, 5: 30, 6: 31, 7: 31, 8: 30, 9: 31, 10: 30, 11: 31}; // Addition of days by month, 0 indexed
	var day1, day2;
	var temp1, temp2;
	var year, year2;
	var string = "";
	var leap = function(year) {return year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0);};
	temp1 = Date1.getMonth();
	temp2 = Date2.getMonth();
	day1 = Date1.getDate();
	day2 = Date2.getDate();
	temp1 -= 1;
	// Leap Year correction
	year = Date1.getFullYear();
	if (temp1 > 2 && leap(year)) {
		day1 += 1;
	}
	year = Date2.getFullYear();
	if (temp2 > 2 && leap(year)) {
		day2 += 1;
	}
	// Adding months
	while (temp1 != -1)
	{
		day1 += dict[temp1];
		temp1 -= 1;
	}
	temp2 -= 1;
	while (temp2 != -1)
	{
		day2 += dict[temp2];
		temp2--;
	}
	// Adding years
	year = Date1.getFullYear();
	year2 = Date2.getFullYear();
	while (year > year2) {
		day1 += 365;
		if (leap(year)) {
			day1 += 1;
		}
		year -= 1;
	}
	var hours1;
	var hours2;
	var diff;
	// StrBuild
	hours1 = day1 * 24 + Date1.getHours();
	hours2 = day2 * 24 + Date2.getHours();
	if (hours1 - hours2 < 25) 
	{
		// Hours ago
		diff = hours1 * 60 + Date1.getMinutes() - hours2 * 60 - Date2.getMinutes();
		if (diff === 0) {
			string = "Seconds ago";
		}
		else {
			if ((diff / 60) >= 1) {
				string = (Math.floor(diff / 60).toString()) + " hour";
				if ((Math.floor(diff/60) % 60) != 1) {
					string += "s";
				}
				string += " and ";
			}
			string += (diff % 60).toString() + " minute";
			if ((diff % 60) != 1) {
				string += "s";
			}
			string += " ago";
		}
	}
	else 
	{
		if (day1 - day2 < 7) {
			
			diff = hours1 - hours2;
			string = (Math.floor(diff/24)).toString() + " day";
			if ((2 > diff/24 > 1)) {
			} else {
				string += "s";
			}
			string += " and " + (diff % 24).toString() + " hour";
			if ((diff % 24) != 1) {
				string += "s";
			}
			string += " ago";
		}
		else 
		{
			string = (day1 - day2).toString() + " days ago";
		}
	}
	return string;
}

function generateTable(created, modified, docLength, word, order)
{
  if (order === undefined) {
    order = [1,2,3,4];
  }
  var titles = ["Created", "Modified", "length", ""];
	var list = fileNames();
	var length = list.length;
	var table = document.createElement('table');
	var row = document.createElement('tr');
	var string;
	var now = new Date();
	var then;
	appendHeader(row, 'File Name');
	if (created) {
	  
	}
	appendHeader(row, 'Modified');
	table.appendChild(row);
	for (i=0; i < length; i++)
	{
		row = document.createElement('tr');
		appendRow(row, list[i]);
		string = localStorage['newtab_save-time-modified-' + list[i]];
		if (string !== undefined) {
			then = new Date(string);
			appendRow(row, timeDif(now, then));
		} else {
			appendRow(row, 'Unknown');
		}
		table.appendChild(row);
	}
	return table;
}
function appendHeader(row, text)
{
	var head = document.createElement('th');
	var p = document.createTextNode(text);
	head.appendChild(p);
	row.appendChild(head);
}
function appendRow(row, text)
{
	var head = document.createElement('td');
	var p = document.createTextNode(text);
	head.appendChild(p);
	row.appendChild(head);
}

function opacityAnimate(hiddenObject, visibleObject, duration)
{
	// Should work in FireFox 37+, Chrome 42+, Opera 29+
	if (duration === undefined) {
		duration = 250;
	}
	hide(visibleObject, duration);
	return show(hiddenObject, duration);
}

function show(Object, duration) {
	if (duration === undefined) {
		duration = 250;
	}
	var animation;
	Object.hidden = 0;
	animation = Object.animate([{opacity: 0}, {opacity: 1}], duration);
	return animation;
}
function hide(Object, duration) {
	if (duration === undefined) {
		duration = 250;
	}
	var animation;
	Object.hidden = 0;
	animation = Object.animate([{opacity: 1}, {opacity: 0}], duration);
	animation.onfinish = function () {Object.hidden = 1;};
	return animation;
}