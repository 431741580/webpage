const main_padding = 22;
const options_padding = 20;
const left_padding = 30;

function tabPage(n) 
{
	var items = left_Section.children.Link_Container.children;
	var l = items.length;
	var active;
	var active_animation;
	var focus = items[n];
	var focus_animation;
	var duration = 250;
	var rect;
	for (x = 0; x < l; x++)
	{
		if (items[x].hidden === false) {
			active = items[x];
		}
	}
	if (active != focus) {
		rect = active.getBoundingClientRect();
		focus.style.position = "absolute";
		active.style.position = "absolute";
		focus.style.left = rect.left + "px";
		focus.style.top = rect.top + "px";
		active.style.left = rect.left + "px";
		active.style.top = rect.top + "px";
		focus.style.zIndex = 1;
		active.style.zIndex = 0;
		active_animation = opacityAnimate(focus, active, duration);
		active_animation.onfinish = function () {
			active.style.position = "static";
			active.style.top = "0px";
			active.style.left = "0px";
			focus.style.position = "static";
			focus.style.top = "0px";
			focus.style.left = "0px";
		};
		localStorage.setItem("newtab_tabPage", n);
	}
}



function fixedFont()
{
	var button = document.getElementById("option2");
	if (window.Main.style.fontFamily === "")
	{
		window.Main.style.fontFamily = "Consolas, Segoe UI Semibold, Segoe UI, Ubuntu, Times New Roman";
		button.innerText = "Serif Font";
	}
	else
	{
		window.Main.style.fontFamily = "";
		button.innerText = "Mono Font";
	}
}
function sideHide()
{
	
	var button = window.options_bar.children[0];
	var main_animation;
	var options_animation;
	var left_animation;
	var duration = 650;
	var options_offsetHeight = window.options_bar.offsetHeight;
	var main_offsetHeight = window.Main.offsetHeight;
	var screenWidth = window.innerWidth;
	var offset = screenWidth * 0.25 + 30;
	var active;
	var len = window.primaryContent.children.length;
	active = primaryContent
	active.style.position = "absolute";
	window.options_bar.style.position = "absolute";
	active.style.left = offset + "px";
	window.options_bar.style.left = offset + "px";
	active.style.top = options_offsetHeight + "px";
	var main_partial = screenWidth - offset - main_padding + "px";
	var main_full = screenWidth - main_padding + "px";
	var options_partial = screenWidth - offset - options_padding + "px";
	var options_full = screenWidth - options_padding + "px";
	
	
	function finish() 
	{
		active.style.position = "static";
		window.options_bar.style.position = "static";
		active.style.left = "0px";
		window.options_bar.style.left = "0px";
		active.style.top = "0px";
	}
	offset += "px";
	if (window.left_Section.hidden === false)
	{
		
		button.innerText = "Expand";
		main_animation = active.animate([{width: main_partial, left: offset}, {width: main_full, left: "0px"}], duration);
		options_animation = window.options_bar.animate([{width: options_partial, left: offset}, {width: options_full, left: "0px"}], duration);
		main_animation.onfinish = function (e) {active.className = "content_container_long";};
		options_animation.onfinish = function (e) {window.options_bar.className = "option_Holder_long";finish();};
		hide(window.left_Section, duration);
	}
	else
	{
		button.innerText = "Retract";
		main_animation = active.animate([{width: main_full, left: "0px"}, {width: main_partial, left: offset}], duration);
		options_animation = window.options_bar.animate([{width: options_full, left: "0px"}, {width: options_partial, left: offset}], duration);
		main_animation.onfinish = function (e) {active.className = "content_container_short";};
		options_animation.onfinish = function (e) {window.options_bar.className = "option_Holder_short";finish();};
		show(window.left_Section, duration);
	}
}
function resetMain()
{
	var bool;
	if (window.global_saved === false) {
		bool = confirm("Reset Text?");
	} else {
		bool = true;
	}
	if (bool)
	{
		window.Main.innerHTML = window.global_defaultstring;
		localStorage.setItem("newtab_text", window.global_defaultstring);
		window.global_saved = true;
		window.global_file_loaded = false;
	}
}
function textWait()
{
	window.global_saved = false;
	if (global_textTimer === true) {}
	else
	{
		window.global_textTimer = true;
		if (window.global_context_used === false) {
			window.context.innerHTML = "Saving Changes";
			window.global_context_used = "text_save";
		}
		window.setTimeout(textWrite,3000);
	}
}
function textWrite()
{
	localStorage.setItem('newtab_text', window.Main.innerHTML);
	console.log("Local storage write");
	window.global_textTimer = false;
	window.setTimeout(function(){if(global_textTimer){}else if (global_context_used ===	 "text_save") {context.innerHTML='Changes Saved';global_context_used = false;}},1000);
	window.setTimeout(function(){if(global_textTimer){}else if (global_context_used === false) {context.innerHTML='';}},6000);
	
}
function search()
{
	var b = document.getElementById("searchBar").value;
	var a = window.aTestHttps.checked;
	if (b === "") {
		window.Explorer.appendChild(generateTable());
	} else {
		aTest(a, b);
	}
			
	/*
	var e = document.getElementById("searchOption");
	var a = e.selectedIndex;
	var b = document.getElementById("searchBar").value;
	var c;
	while (b === "")
	{
		b = prompt("Please enter a query \nSearch Provider: " + e.options[a].textContext);
	}
	if (b !== null) 
	{
		switch (a) {
			case 0:
				c = "https://www.google.com.au/#q=" + encodeURIComponent(b);
				break;
			case 1:
				c = "http://www.wolframalpha.com/input/?i=" + encodeURIComponent(b);
				break;
			case 2:
				c = encodeURI("https://www.youtube.com/results?search_query=" + b);
				break;
			case 3:
				c = encodeURI("http://tvtropes.org/pmwiki/search_result.php?q=" + b);
				break;
			case 4:
				if (isNaN(parseInt(b, 10)))
				{	
					c = encodeURI("http://www.explainxkcd.com/wiki/index.php?title=Special:Search&search="+ b);
				} else {
					c = encodeURI("http://www.explainxkcd.com/wiki/index.php/ + b");
				}
				break;
			case 5:
				c = encodeURI("http://terraria.gamepedia.com/index.php?title=Special:Search&search=" + b);
				break;
			default:
				alert("object <optionSelect> has unexpected value: " + a);
				c = undefined;
				break;
		}
		if (c !== undefined) {
			window.open(c);
		}
	}
	*/
	
}
function inputList(s, l, p, f, d) 
{
	if (!d || d === false) {
		d = "";
	}
	var userInput = prompt(s, d);
	var valid;
	
	while (userInput === "") {
		userInput = prompt("Missing Input\n" + s);
	}
	if (userInput !== null && f(userInput, l) && userInput != d) {
		valid = confirm(p);
		if (valid) { 
		return userInput; 
		} else { 
		return inputList(s, l, p, f);
		}
	} else if (userInput !== null) { 
	  return userInput;
	}
}
function textSave(name)
{
	var list = fileNames();
	var re = /[^0-9A-z ]/;
	var overwriteString = "File with name already exists\nOk to overwrite?";
	var f = function(s,l) {if (l.indexOf(s) === -1) {return false;} else {return true;}};
	if (name === undefined || typeof(name) != "string") {
		name = inputList("Please enter a name", list, overwriteString, f, global_file_loaded);
	}
	while (re.test(name)) {
		name = inputList("Names must be alphanumeric, spaces permitted", list, overwriteString, f);
	}
	if (name !== null) 
	{
		// Meta data storage
		var d;
		var l;
		var words = 0;
		var chars = 0;
		var length;
		var l2;
		var length2;
		l = window.Main.innerText.split("\n");
		length = l.length;
		for (i = 0; i < length; i++) {
			if (l[i] !== '') {
				l2 = l[i].split(' ');
				length2 = l2.length;
				for (o = 0; o < length2; o++) {
					if (l2[o] !== '') {
						words += 1;
					}
				}
				chars += l[i].split('').length;
			}
		}
		
		d = new Date();
		localStorage.setItem("newtab_save-item-" + name, window.Main.innerHTML);
		if (name != global_file_loaded) {
			localStorage.setItem("newtab_save-time-create-" + name, d.toJSON()) ;
			}
		localStorage.setItem("newtab_save-time-modified-" + name, d.toJSON());
		localStorage.setItem("newtab_save-length-" + name, chars);
		localStorage.setItem("newtab_save-word-count-" + name, words);
		// Storage of name to index
		if (list.indexOf(name) == -1)
		{
			string = localStorage['newtab_save-name'];
			if (string === null || string === "") {
				localStorage.setItem("newtab_save-name", name);
			} else {
				localStorage.setItem("newtab_save-name", string + "~" + name);
			}
		}
		// END meta information
		window.global_saved = true;
		window.global_file_loaded = name;
		if (global_context_used === false) {
			window.context.innerText = "File Saved";
			window.global_context_used = "file_save";
			window.setTimeout(function() {if (global_context_used === "file_save") {global_context_used = false;}}, 1000);
			window.setTimeout(function() {if (global_context_used === false) {context.innerText = "";}}, 3000);
		} else {
			alert("File saved");
		}
	}
}	
function textLoad(name)
{
	var bool;
	if (global_saved === false) {
		bool = confirm("Overwrite text?");
	} else { bool = true;}
	if (bool) {
		var list = fileNames();
		if (name === undefined || typeof(name) != "string") {
			if (global_file_loaded === null || global_file_loaded === false) 
			{
				name = prompt("Please enter the file name");
			}
			else
			{
				name = prompt("Please enter the file name", global_file_loaded);
			}
		}
		else {
		}
		while (list.indexOf(name) === -1 && name !== null) {
			name = prompt("Error -41: File not found\nPlease enter the file name");
		}
		if (name !== null) 
		{
			if (list.indexOf(name) != -1) {
				var a = new Date();
				var c = localStorage["newtab_save-time-modified-" + name];
				if (c !== null)
				{
					var b = new Date();
					window.context.innerText = "File Modified: " + timeDif(a,b);
				}
				else
				{
					window.context.innerText = "File Loaded";
				}
				
			}
			else {
				window.context.innerText = "File Loaded";
			}
			window.global_saved = true;
			window.global_file_loaded = name;
			if (window.global_context_used === false) {
				window.global_context_used = "file_load";
				window.setTimeout(function() {if (global_context_used === "file_load") {global_context_used = false;}}, 500);
				window.setTimeout(function() {if (global_context_used === false) {context.innerText = "";}}, 7000);
				window.Main.innerHTML = localStorage.getItem("newtab_save-item-" + name);
			} 
		}
	}
}
function textRemove(name)
{
	var bool;
	var list = fileNames();
	var s = "";
	var len;
	if (name === undefined || typeof(name) != "string") {
		name = prompt("Enter file to remove"); }
	while (list.indexOf(name) == -1 && name !== null) 
	{
		name = prompt("Error -41: File not found\nEnter file to remove");
	}
	if (name !== null) 
	{
		localStorage.removeItem("newtab_save-item-" + name);
		localStorage.removeItem("newtab_save-time-modified-" + name);
		localStorage.removeItem("newtab_save-time-create-" + name);
		localStorage.removeItem("newtab_save-length-" + name);
		localStorage.removeItem("newtab_save-word-count-" + name);
		list.splice(list.indexOf(name), 1);
		len = list.length - 1;
		for (i = 0; i < len; i++) {
			s += list[i] + "~";
		}
		if (list.length > 0) {
			s += list[i]; }
		localStorage.setItem("newtab_save-name", s);
		if (window.global_context_used === false) {
			window.context.innerText = "File Removed";
			window.global_context_used = "file_remove";
			window.setTimeout(function() {if (global_context_used === "file_remove") {global_context_used = false;}}, 1000);
			window.setTimeout(function() {if (global_context_used === false) {context.innerText = "";}}, 3000);
		} else {
			alert("File removed");
		}
	}
}

function timeDif(Date1, Date2)
{
	// Date1 has to be larger than Date2
	var dict = {0: 31, 1: 28, 2: 31, 3: 30, 4: 31, 5: 30, 6: 31, 7: 31, 8: 30, 9: 31, 10: 30, 11: 31}; // Addition of days by month, 0 indexed
	var day1, day2;
	var temp1, temp2;
	var year, year2;
	var string = "";
	var leap = function(year) {return year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0);};
	temp1 = Date1.getMonth();
	temp2 = Date2.getMonth();
	day1 = Date1.getDate();
	day2 = Date2.getDate();
	temp1 -= 1;
	// Leap Year correction
	year = Date1.getFullYear();
	if (temp1 > 2 && leap(year)) {
		day1 += 1;
	}
	year = Date2.getFullYear();
	if (temp2 > 2 && leap(year)) {
		day2 += 1;
	}
	// Adding months
	while (temp1 != -1)
	{
		day1 += dict[temp1];
		temp1 -= 1;
	}
	temp2 -= 1;
	while (temp2 != -1)
	{
		day2 += dict[temp2];
		temp2--;
	}
	// Adding years
	year = Date1.getFullYear();
	year2 = Date2.getFullYear();
	while (year > year2) {
		day1 += 365;
		if (leap(year)) {
			day1 += 1;
		}
		year -= 1;
	}
	var hours1;
	var hours2;
	var diff;
	// StrBuild
	hours1 = day1 * 24 + Date1.getHours();
	hours2 = day2 * 24 + Date2.getHours();
	if (hours1 - hours2 < 25) 
	{
		// Hours ago
		diff = hours1 * 60 + Date1.getMinutes() - hours2 * 60 - Date2.getMinutes();
		if (diff === 0) {
			string = "Seconds ago";
		}
		else {
			if ((diff / 60) >= 1) {
				string = (Math.floor(diff / 60).toString()) + " hour";
				if ((Math.floor(diff/60) % 60) != 1) {
					string += "s";
				}
				string += " and ";
			}
			string += (diff % 60).toString() + " minute";
			if ((diff % 60) != 1) {
				string += "s";
			}
			string += " ago";
		}
	}
	else 
	{
		if (day1 - day2 < 7) {
			
			diff = hours1 - hours2;
			string = (Math.floor(diff/24)).toString() + " day";
			if ((2 > diff/24 > 1)) {
			} else {
				string += "s";
			}
			string += " and " + (diff % 24).toString() + " hour";
			if ((diff % 24) != 1) {
				string += "s";
			}
			string += " ago";
		}
		else 
		{
			string = (day1 - day2).toString() + " days ago";
		}
	}
	return string;
}
function fileNames()
{
	var string = localStorage.getItem("newtab_save-name");
	if (string === null || string === '') {
		return [];
	} else  {
		return string.split("~");
	}
}

function generateTable(created, modified, docLength, word, order)
{
  if (order === undefined) {
    order = [1,2,3,4];
  }
  var titles = ["Created", "Modified", "length", ""];
	var list = fileNames();
	var length = list.length;
	var table = document.createElement('table');
	var row = document.createElement('tr');
	var string;
	var now = new Date();
	var then;
	appendHeader(row, 'File Name');
	if (created) {
	  
	}
	appendHeader(row, 'Modified');
	table.appendChild(row);
	for (i=0; i < length; i++)
	{
		row = document.createElement('tr');
		appendRow(row, list[i]);
		string = localStorage['newtab_save-time-modified-' + list[i]];
		if (string !== undefined) {
			then = new Date(string);
			appendRow(row, timeDif(now, then));
		} else {
			appendRow(row, 'Unknown');
		}
		table.appendChild(row);
	}
	return table;
}

function appendHeader(row, text)
{
	var head = document.createElement('th');
	var p = document.createTextNode(text);
	head.appendChild(p);
	row.appendChild(head);
}
function appendRow(row, text)
{
	var head = document.createElement('td');
	var p = document.createTextNode(text);
	head.appendChild(p);
	row.appendChild(head);
}

function opacityAnimate(hiddenObject, visibleObject, duration)
{
	// Should work in FireFox 37+, Chrome 42+, Opera 29+
	if (duration === undefined) {
		duration = 250;
	}
	hide(visibleObject, duration);
	return show(hiddenObject, duration);
}

function show(Object, duration) {
	if (duration === undefined) {
		duration = 250;
	}
	var animation;
	Object.hidden = 0;
	animation = Object.animate([{opacity: 0}, {opacity: 1}], duration);
	return animation;
}
function hide(Object, duration) {
	if (duration === undefined) {
		duration = 250;
	}
	var animation;
	Object.hidden = 0;
	animation = Object.animate([{opacity: 1}, {opacity: 0}], duration);
	animation.onfinish = function () {Object.hidden = 1;};
	return animation;
}
function navigate() {
	var options_offsetHeight = window.options_bar.offsetHeight;
	var a = parseInt(tileNavigation.value);
	var active;
	var focus;
	var duration = 200;
	var animation;
	var itemArray;
	var left_width = window.innerWidth * 0.25 + left_padding;
	switch (a) {
		case 0:
			active = window.Main;
			break;
		case 1:
			active = window.Explorer;
			break;
	}
	switch (global_current_tile) {
		case 0:
			focus = window.Main;
			break;
		case 1:
			focus = window.Explorer;
			break;
	}
	if (tileNavigation.value === 1) {
		loadExplorer();
	}
	itemArray = [active, focus];
	
	window.requestAnimationFrame(function () {
		for (i = 0; i < 2; i++) 
		{
			itemArray[i].style.position = "absolute";
			itemArray[i].style.top = options_offsetHeight + "px";
			itemArray[i].style.left = left_width + "px";
		}
	});
	//console.log(active, focus)
	animation = opacityAnimate(active, focus, duration);
	global_current_tile = a;
	tileNavigation.value = a;
	animation.onfinish = function () {
		active.style.position = "static";
		focus.style.position = "static";
		//resetDisplay();
	};
}
function loadExplorer () {
	explorer.innerHTML = "";
	window.requestAnimationFrame(function () { explorer.appendChild(generateTable());});
}

function aTest(https, Link) {
	var string;
	if (https === undefined){
		https = true;
	}
	if (Link === undefined) {
		string = prompt("Enter URL");
	} else {
		string = Link;
	}
	var a;
	var b;
	var c;
	if (string !== null) {
		a = document.createElement('a');
		var href;
		href = "http";
		if (https) {
			href += "s";
		}
		href += "://";
		href += string.slice(7);
		a.href = href;
		a.setAttribute('download', true);
		b = document.createTextNode('Output');
		a.appendChild(b);
		Link3.appendChild(a);
		Link3.appendChild(document.createElement('br'));
	}
}
 
function NotificationTest() {
	var titleString = "Hey! Link!";
	var notification
	if (Notification.permission === "granted") 
	{
		// If it's okay let's create a notification
		notification = new Notification(titleString);
	}

	// Otherwise, we need to ask the user for permission
	else if (Notification.permission !== 'denied') 
	{
		Notification.requestPermission(function (permission)
		{
			// If the user accepts, let's create a notification
			if (permission === "granted") {
				notification = new Notification(titleString);
			}
		} 
		);
	}
	return notification;
}

function resetDisplay()
{
	var hidden = window.left_Section.hidden;
	var list = window.primaryContent.children;
	var len = list.length;
	var active;
	for (i=0; i < len; i++)
	{
	  if (list[i].hidden === false)
	  {
	    active = list[i];
	  }
	}
	if (hidden) {
	  active.className = "content_container_long";
	} else {
	  active.className = "content_container_short";
	}
}

function renderExplorer()
{
  var screen = document.getElementById("Explorer");
  var renderObject = document.createDocumentFragment();
  for (i = screen.childNodes.length - 1; i >= 0; i--) {
    screen.childNodes[i].remove();
  }
  var table = generateTable();
  table.className = "ExplorerTable";
  renderObject.appendNode(table);
  
}
const dialogShow = function _dialogShowWrapper () {
	var queue = [];
	return function (
		text, 
		prompt, 
		checkBoxBox, 
		trueHandler, 
		falseHandler,
		autofill, 
		checkBoxText, 
		checkBoxDisable,
		confirmBoxDisable ) {
	/* 
	text     	 : document.createTextNode / string
	prompt   	 : boolean
	checkBox 	 : boolean
	trueHandler	 : function - takes a string if prompt, + boolean if checkBox, as parameter
	falseHandler : function
	autofill 	 : string (optional)
	checkBoxText : string (optional)
	checkBoxDisable	 : function (optional), takes a string as parameter, returns 1 or 0 for active or disabled
	confirmBoxDisable	 : function (optional), takes string and boolean as parameter, returns 1 or 0 for active or disabled
	*/
	checkBox = checkBoxBox;
	var l = ConfirmDialog.children;
	var CheckboxActive = 1;
	var disableCheckbox = function (active) {
		if (CheckboxActive !== active) {
			if (active) {
				l[2].children[0].style.color = "rgb(0, 0, 0)";
				l[2].children[1].disabled = 0;
			} else {
				l[2].children[0].style.color = "rgba(0, 0, 0, .3)";
				l[2].children[1].disabled = 1;
			}
			CheckboxActive = active;
		}
	};		
			
	
	
	var trueClick = function () {
		console.log(prompt, checkBox);
		if (prompt && checkBox) {
			trueHandler(l[1].value, l[2].children[1].checked);
		} else if (prompt) {
			trueHandler(l[1].value);
		} else if (checkBox) {
			trueHandler(l[2].children[1].check);
		} else {
			trueHandler();
		}
		ConfirmDialog.close();
		l[4].removeEventListener('click');
		l[5].removeEventListener('click');
		l[1].removeEventListener('keyup');
		l[2].children[1].removeEventListener('keyup');
	};
	
	var falseClick = function  () {
		falseHandler();
		ConfirmDialog.close();
		l[4].removeEventListener('click');
		l[5].removeEventListener('click');
		l[1].removeEventListener('keyup');
		l[2].children[1].removeEventListener('keyup');
	};
	
	var disableConfirm = function (active) {
		if (active) {
			l[4].disabled = 0;
		} else {
			l[4].disabled = 1;
		}
	};
	
	var checkInput = function () {
		disableCheckbox(checkBoxDisable(l[1].value));
		if (confirmBoxDisable !== undefined) {
			disableConfirm(confirmBoxDisable(l[1].value, l[2].children[1].checked));
		}
	};
	
	var checkConfirm = function () {
		disableConfirm(confirmBoxDisable(l[1].value, l[2].children[1].checked));
	};
	
	if (typeof(text) === "string") {
		text = document.createTextNode(text);
	};
	
	var enter = function (e) {
		if (((e.which || e.keyCode) === 13) && !l[4].disabled && ConfirmDialog.open) {
			console.log(e.keyCode);
			trueClick();
		}
	};
	
	/*
	l[0] = Central Text
	l[1] = Prompt
	l[2] = Checkbox Div
	l[3] = <br>
	l[4] = Confirm Button
	l[5] = Cancel Button
	*/
	l[1].removeEventListener('keyup');
	l[1].removeEventListener('input');
	l[2].children[1].removeEventListener('change');
	l[2].children[1].checked = false;
	l[2].children[1].innerHTML = "";
	l[4].removeEventListener('click');
	l[5].removeEventListener('click');
	l[1].removeEventListener('keyup');
	l[2].children[1].removeEventListener('keyup');
	l[0].innerHTML = "";
	l[0].appendChild(text);
	disableConfirm(1);
	l[1].value = "";
	if (confirmBoxDisable !== undefined) {
		disableCheckbox(1);
	} else {
		disableCheckbox(0);
	}
	if (prompt) {
		l[1].hidden = 0;
		if (typeof(autofill) === "string") {
			l[1].placeholder = autofill;
		}
	} else {
		l[1].hidden = 1;
	}
	
	l[1].addEventListener('keypress', enter);
	l[2].children[1].addEventListener('keypress', enter);
	if (checkBox) {
		l[2].hidden = 0;
		
		if (checkBoxText !== undefined) {
			l[2].children[0].innerText = checkBoxText;
		} else {
			l[2].children[0].innerText = "";
		}
		
		if (checkBoxDisable !== undefined) {
			l[1].addEventListener('input', checkInput);
			checkInput();
		}
		if (confirmBoxDisable !== undefined) {
			l[1].addEventListener('input', checkConfirm);
			l[2].children[1].addEventListener('change', checkConfirm);
			checkConfirm();
		}
		if (checkBoxDisable !== undefined && confirmBoxDisable !== undefined) {
			l[2].children.disabled = false;
		}
	} else {
		l[2].hidden = 1;
	}
	
	l[4].onclick = trueClick;
	l[5].onclick = falseClick;
	ConfirmDialog.showModal()
	return true;
};

}()

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

// New implementation to work with less retarded pop-ups
function textSaveInit(name)
{
	var list = fileNames();
	var re = /[^0-9A-z ]/;
	var overwriteString = "File with name already exists\nOk to overwrite?";
	var checkOverwrite = function (name) {
		if (list.indexOf(name) === -1 && name !== global_file_loaded) {
			return 0;
		} else {
			return 1;
		}
	};
	var confirmCheck = function (name, bool) {
			if ((list.indexOf(name) === -1 || name === global_file_loaded || bool)
				&& (!re.test(name) && name !== '')) {
				return 1;
			} else {
				return 0;
			}
		}
	if (name === undefined || typeof(name) != "string") {
		dialogShow("Save File as:", true, true, textSaveResponse, 
					function () {}, global_file_loaded || "", 
					"Overwrite File?", checkOverwrite, confirmCheck);
	} else {
		textSaveResponse(name, true)
	}
}


function textSaveResponse(name, confirm) {
	
	var list = fileNames();
	var re = /[^0-9A-z ]/;
	var overwriteString = "File with name already exists\nOk to overwrite?";
	if (re.test(name) || name === '' ) 
	{
		var checkOverwrite = function (name) {
			if (list.indexOf(name) === -1 && name !== global_file_loaded) {
				return 0;
			} else {
				return 1;
			}
		};
		var confirmCheck = function (name, bool) {
			if (list.indexOf(name) === -1 && name !== global_file_loaded || bool) {
				return 1;
			} else {
				return 0;
			}
		};
		
		dialogShow("Save File as:", true, true, textSaveResponse, 
					function () {}, "", "Overwrite File?", 
					checkOverwrite, confirmCheck);
	} 
	else if (list.indexOf(name) === -1 || confirm === true)
	{
		// Do stuff
		// Meta data storage
		var d;
		var l;
		var words = 0;
		var chars = 0;
		var length;
		var l2;
		var length2;
		l = window.Main.innerText.split("\n");
		length = l.length;
		for (i = 0; i < length; i++) {
			if (l[i] !== '') {
				l2 = l[i].split(' ');
				length2 = l2.length;
				for (o = 0; o < length2; o++) {
					if (l2[o] !== '') {
						words += 1;
					}
				}
				chars += l[i].split('').length;
			}
		}
		
		d = new Date();
		localStorage.setItem("newtab_save-item-" + name, window.Main.innerHTML);
		if (name != global_file_loaded) {
			localStorage.setItem("newtab_save-time-create-" + name, d.toJSON()) ;
			}
		localStorage.setItem("newtab_save-time-modified-" + name, d.toJSON());
		localStorage.setItem("newtab_save-length-" + name, chars);
		localStorage.setItem("newtab_save-word-count-" + name, words);
		// Storage of name to index
		if (list.indexOf(name) == -1)
		{
			string = localStorage['newtab_save-name'];
			if (string === null || string === "") {
				localStorage.setItem("newtab_save-name", name);
			} else {
				localStorage.setItem("newtab_save-name", string + "~" + name);
			}
		}
		// END meta information
		window.global_saved = true;
		window.global_file_loaded = name;
		if (global_context_used === false) {
			window.context.innerText = "File Saved";
			window.global_context_used = "file_save";
			window.setTimeout(function() {if (global_context_used === "file_save") {global_context_used = false;}}, 1000);
			window.setTimeout(function() {if (global_context_used === false) {context.innerText = "";}}, 3000);
		} else {
			alert("File saved");
		}
	}
}

function textLoadInit() {
	var list = fileNames();
	function confirmCheck (name, bool) {
		if (list.indexOf(name) !== -1 && (global_saved || bool)) {
			return 1;
		} else {
			return 0;
		}
	};
	
	var a = function () {};
	var b = function (name) {
		if (list.indexOf(name) !== -1) {
			return 1;
		} else {
			return 0;
		}
	}
	if (!global_saved) {
		dialogShow("File Name:", true, true, textLoadResponse, 
					a, "", "Overwrite file?", b, confirmCheck);
	} else { 
		dialogShow("File Name:", true, false, textLoadResponse, 
					a, "", "", b, confirmCheck);
	}
}
		
		
		
	
function textLoadResponse(name, confirm) {
	console.log(name, confirm	);
	if (confirm === undefined) {
		confirm = true;
	}
	if (name === '') {
		confirm = false;
	}
	if (confirm) {
		var list = fileNames()
		// Read data
		if (list.indexOf(name) != -1) {
			var a = new Date();
			var c = localStorage["newtab_save-time-modified-" + name];
			if (c !== undefined)
			{
				var b = new Date();
				window.context.innerText = "File Modified: " + timeDif(a,b);
			}
			else
			{
				window.context.innerText = "File Loaded";
			}
			
		}
		else {
			window.context.innerText = "File Loaded";
		}
		window.global_saved = true;
		window.global_file_loaded = name;
		if (window.global_context_used === false) {
			window.global_context_used = "file_load";
			window.setTimeout(function() {if (global_context_used === "file_load") {global_context_used = false;}}, 500);
			window.setTimeout(function() {if (global_context_used === false) {context.innerText = "";}}, 7000);
		}
		window.Main.innerHTML = localStorage.getItem("newtab_save-item-" + name);
	}
}

function resetMainInit() {
	
	if (!window.global_saved) {
		dialogShow("Reset Text?", false, false, resetMainResponse, function() {return 1;})
	}
	else {
		resetMainResponse();
	}
}

function resetMainResponse() {
	window.Main.innerHTML = window.global_defaultstring;
	localStorage.setItem("newtab_text", window.global_defaultstring);
	window.global_saved = true;
	window.global_file_loaded = false;
}

function textRemoveInit() {
	var list = fileNames();
	var len;
	var check = function (name) {
		if (list.indexof(name) === -1) {
			return false;
		} else { 
			 return true;
		}
	}
	dialogShow("File to Remove: ", true, false, textRemoveResponse, function () {}, "", "", function () {}, check);
}

function textRemoveResponse (name) {
	localStorage.removeItem("newtab_save-item-" + name);
	localStorage.removeItem("newtab_save-time-modified-" + name);
	localStorage.removeItem("newtab_save-time-create-" + name);
	localStorage.removeItem("newtab_save-length-" + name);
	localStorage.removeItem("newtab_save-word-count-" + name);
	list.splice(list.indexOf(name), 1);
	len = list.length - 1;
	for (i = 0; i < len; i++) {
		s += list[i] + "~";
	}
	if (list.length > 0) {
		s += list[i]; }
	localStorage.setItem("newtab_save-name", s);
	if (window.global_context_used === false) {
		window.context.innerText = "File Removed";
		window.global_context_used = "file_remove";
		window.setTimeout(function() {if (global_context_used === "file_remove") {global_context_used = false;}}, 1000);
		window.setTimeout(function() {if (global_context_used === false) {context.innerText = "";}}, 3000);
	} else {
		alert("File removed");
	}
}

function dialogShowDown(e) {
	var propreties = getComputedStyle(ConfirmDialog);
	var dialogTop = parseInt(propreties.top.slice(0, -2));
	var dialogLeft = parseInt(propreties.left.slice(0,-2));
	var X = e.layerX
	var Y = e.layerY
	//console.log(top, left, X, Y)
	var a = (dialogTop + Y) + "px";
	var b = (dialogLeft + X) + "px";
	console.log(a,b);
	ConfirmDialog.style.top = a;
	ConfirmDialog.style.left = b;
}




